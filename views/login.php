<!DOCTYPE html>
<html>
  <head>

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  </head>
<body id="LoginForm" style="background-color: #232323;">
<div class="container" style="width: 100%; height: 100vh;">
<div class="login-form" style="text-align: center; width: 100%; height: 100%;">
<div class="main-div" style="width: 312px; margin: 0 auto; padding: 32px; background-color: white; border-radius: 5px; margin-top: 64px">
    <div class="panel">
   <h2>Вход</h2>
   <p>Пожалуйста, введите <b>имя пользователя</b> и <b>пароль</b></p>
   </div>
    <form method="post" action="../controllers/loginController.php">

        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Имя пользователя">
        </div>

        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Пароль">
        </div>
        <div class="forgot">
        <input type="submit" class="btn btn-primary" name="loginSubmit" value="Войти">
    </div>

    </form>
</div></div></div>
</body>
</html>