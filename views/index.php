<?php 
session_start(); 
if(!isset($_SESSION['username'])){
    //go to login.php to log in
    header('Location: login.php');
    die();
}
else {
    ?>
    <!DOCTYPE html> 
<html>
 <?php
     include 'header.php';    
     $IdSession=$_SESSION['id'];
     ?>
  
<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container" >
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="font-weight: bolder; color: white">МАГАЗИН</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">

		<li>
          <a href="index.php">Главная</a>
        </li>

<!-- 				<li class="dropdown">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Товары<b class="caret"></b></a>
	        <ul class="dropdown-menu"> -->
	          <li><a href="?page=products&id=1">Товары</a></li>
	          <!-- <li><a href="?page=products&id=3">Книги</a></li> -->
	       <!--  </ul>
	      </li> -->
        <li>
          <a href="?page=products&id=2">Сервис</a>
        </li>

		<li><a href="?page=mybbasket&id=<?php echo$_SESSION['id'];?>">Корзина</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">

<li class="dropdown">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Привет, <?php  echo " ".$_SESSION['username'];?>!<b class="caret"></b></a>
	        <ul class="dropdown-menu">
	          <li><a href="../controllers/logout.php"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
	        </ul>
	      </li>
    </ul>
    </nav>
  </div>
</header>
    <body style="background: #AAB444">
<div class="container" ng_view>
  
</div>
        <header>
        </header>
        <main>
            <section>
                <!--to open page form url-->
             <?php if(isset($_GET['page'])){
              $page= $_GET['page'];
              include '../controllers/getDataController.php';
              include $page.'.php';
                }
               else{
                echo '
                	<div style="background: #AAB444; width: 100%; height: 100%"> 
                	 <h3 style="text-align: center">'
                . 'Добро пожаловать! '
                 . '</h3>
                	<img src="images/bg.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 60%;">
                 	</div>
                 ';
                }
                ?>
            </section>
        </main>
        <footer>
          <?php 
            include 'footer.php';
          ?>
        </footer>
        <script src="js/codejs.js" type="text/javascript"></script>
    </body>
</html>
   <?php }
?>